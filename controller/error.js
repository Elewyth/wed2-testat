'use strict';

const logger = require('logops');

// Express explicitly requires 4 parameters on an error handler
//noinspection JSUnusedLocalSymbols
exports.error = function(error, request, response, next) {
	logger.error('%s', error.stack);
	response.status(500);
	response.render('error', { error: error });
};