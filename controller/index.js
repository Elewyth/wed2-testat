'use strict';

const moment = require('moment');
const noteService = require('../services/note-service');

exports.get = function (request, response) {

    setSessionVariable(request, 'asc',  stringToBoolean(request.query['asc']));
    setSessionVariable(request, 'showUnfinished', stringToBoolean(request.query['showUnfinished']));
    setSessionVariable(request, 'sort', request.query['sort']);
    setSessionVariable(request, 'style', request.query['style']);

    const asc = request.session['asc'];
    const showUnfinished = request.session['showUnfinished'];
    const sort = request.session['sort'];
    const style = request.session['style'];

    const callback = function (error, notes) {
        for (let note of notes) {
            note.dueDateText = getRelativeDueDateText(note.dueDate);
        }
        response.render('index', {notes: notes, asc: !asc, style: style, showAll: !showUnfinished});
    };

    if (showUnfinished) {
        noteService.findUnfinishedNotes(sort, asc, callback);
    } else {
        noteService.findAllNotes(sort, asc, callback);
    }
};

const getRelativeDueDateText = function (dueDate) {
    if (!dueDate) {
        return '';
    }
    const dueMoment = moment(dueDate);
    return dueMoment.fromNow();
};

const setSessionVariable = function(request, name, value){
    if(value !== undefined) {
        request.session[name] = value;
    }
};

const stringToBoolean = function(value){
    if(value === undefined || value === null){
        return value;
    } else {
        return value === 'true';
    }
};