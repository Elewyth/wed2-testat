'use strict';

const logger = require('logops');
const noteService = require('../services/note-service');

exports.getCreate = function(request, response) {
    response.render('edit', {style: request.session['style']});
};

const findNoteCallbackWrapper = function(response, noteId, wrappedCallback) {
	return function(error, note) {
		if (note.length === 0) {
			logger.debug('Note with id %s was not found', noteId);
			response.status(404).render('error', { error: 'Note with id ' + noteId + ' was not found' });
			return;
		}

		if (note.length > 1) {
			logger.error('More than one note found for id %s', noteId);
			response.status(500).render('error', { error: 'More than one note found for id ' + noteId });
			return;
		}

		logger.debug('Note found: %j', note[0]);
		wrappedCallback(note[0]);
	};
};

exports.getEdit = function(request, response) {
    const noteId = request.params['noteId'];
	noteService.find(noteId, findNoteCallbackWrapper(response, noteId, function(note) {
		note.dueDateFormatted = formatDateAsISO(note.dueDate);
		response.render('edit', { note: note, style: request.session['style']});
	}));
};

const formatDateAsISO = function(date) {
	const yearString = `0000${date.getFullYear()}`.slice(-4);
	const monthString = `00${date.getMonth() + 1}`.slice(-2);
	const dayString = `00${date.getDate()}`.slice(-2);
	return `${yearString}-${monthString}-${dayString}`;
};

const convertBodyToNote = function(body) {
    return new noteService.Note(body['title'], body['description'], Number(body['importance']), new Date(body['dueDate']), body['finished']);
};

exports.postCreate = function(request, response) {
	logger.debug('Request-Body: %j', request.body);
	let note = convertBodyToNote(request.body);
	if (!note.title) {
		response.status(400).render('edit', { note: note, error: 'Title is required' });
		return;
	}

	noteService.save(note, function() {
		response.redirect(303, '/');
	});
};

exports.postEdit = function(request, response) {
	logger.debug('Request-Body: %j', request.body);
	const noteId = request.params['noteId'];
	noteService.find(noteId, findNoteCallbackWrapper(response, noteId, function() {
		let note = convertBodyToNote(request.body);
		note._id = noteId;
		if (!note.title) {
			response.status(400).render('edit', { note: note, error: 'Title is required' });
			return;
		}

		noteService.save(note, function() {
			response.redirect(303, '/');
		});
	}));
};