'use strict';

//noinspection JSUnusedGlobalSymbols
exports.helpers = {
	for: function(from, to, increment, block) {
		let result = '';
		for(let i = from; i < to; i += increment) {
			result += block.fn(i);
		}
		return result;
	},

	ifEq: function(a, b, block) {
		if (a === b) {
			return block.fn(this);
		}
		return block.inverse(this);
	}
};