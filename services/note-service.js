'use strict';

const Datastore = require('nedb');
const db = new Datastore({ filename: './data/notes.db', timestampData: true, autoload: true });

const Note = function(title, description = '', importance = 1, dueDate = null, finished = false) {
	this.title = title;
	this.description = description || null;
	this.importance = importance || null;
	this.dueDate = dueDate || null;
	this.finished = finished || false;
};

exports.Note = Note;

exports.save = function(note, callback) {
	if (note._id === undefined) {
		db.insert(note, callback);
	} else {
		db.update({ _id: note._id}, note, callback);
	}
};

exports.findAllNotes = function(sortProperty, ascending, callback) {
	let sortQuery = {};
	sortQuery[sortProperty] = ascending ? 1 : -1;

	db.find({})
		.sort(sortQuery)
		.exec(callback);
};

exports.findUnfinishedNotes = function(sortProperty, ascending, callback) {
    let sortQuery = {};
    sortQuery[sortProperty] = ascending ? 1 : -1;

    db.find({ finished: false })
        .sort(sortQuery)
        .exec(callback);
};

exports.find = function(noteId, callback) {
	db.find({ _id: noteId })
		.exec(callback);
};

exports.deleteAllNotes = function(callback) {
	db.remove({}, { multi: true }, callback);
};

exports.delete = function(note) {
	db.remove({ _id: note._id });
};