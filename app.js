'use strict';

const bodyParser = require('body-parser');
const express = require('express');
const exphbs = require('express-handlebars');
const expressLogging = require('express-logging');
const expressSession = require('express-session');
const logger = require('logops');
const handlebars = require('handlebars');

const editController = require('./controller/edit');
const errorHandler = require('./controller/error');
const handlebarsHelpers = require('./services/handlebars-helpers');
const indexController = require('./controller/index');
const noteService = require('./services/note-service');

const hostname = '127.0.0.1';
//noinspection JSUnresolvedVariable
const port = isNaN(process.env.SERVER_PORT) ? 80 : process.env.SERVER_PORT;

// Express setup
const app = express();
app.use(bodyParser.urlencoded({
	extended: true
}));

// Initialize with test data
noteService.deleteAllNotes(function() {
    noteService.save(new noteService.Note(
        "Test Note One",
        "Description of Test Note One",
        3,
        new Date(2017, 10, 18),
        false
    ));
    noteService.save(new noteService.Note(
        "Test Note Two",
        "Description of Test Note Two",
        2,
        new Date(2017, 10, 17),
        true
    ));
    noteService.save(new noteService.Note(
        "Test Note Three",
        "Description of Test Note Three",
        5,
        new Date(2017, 10, 1),
        true
    ));
    noteService.save(new noteService.Note(
        "Test Note Four",
        "Description of Test Note Four",
        1,
        new Date(2017, 10, 19),
        true
    ));
    noteService.save(new noteService.Note(
        "Test Note Five",
        "Description of Test Note Five",
        4,
        new Date(2017, 11, 20),
        true
    ));
});

// Logging setup
logger.format = logger.formatters.dev;
logger.setLevel('DEBUG');
app.use(expressLogging(logger, {
	blacklist: ['/stylesheets']
}));

// Session setup
app.use(expressSession({
    secret: "etwasAnderes",
    resave: false,
    saveUninitialized: true
}));

// Handlebars setup
app.engine('handlebars', exphbs({defaultLayout: 'main', helpers: handlebarsHelpers.helpers}));
app.set('view engine', 'handlebars');

// Controller setup
app.use(express.static('public'));
app.get('/', indexController.get);
app.route('/edit')
	.get(editController.getCreate)
	.post(editController.postCreate);
app.route('/edit/:noteId')
	.get(editController.getEdit)
	.post(editController.postEdit);
app.use(errorHandler.error);

// Start server
app.listen(port, hostname, () => {
	console.log(`Server running at http://${hostname}:${port}/`);
});
